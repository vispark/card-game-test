import React, {useState, useEffect} from 'react';
import cards from '../../constants/cards';

import SuitsSelectorStyled, {SelectorContainer, SuitIcon} from './index.styled';

export default function SuitsSelector({onSuitSelected = () => {}}) {
  const [selectedSuit, setSuit] = useState(cards.suits[0]);

  useEffect(() => {
    selectSuit(cards.suits[0]);
  }, []);

  const selectSuit = (suit) => {
    setSuit(suit);
    onSuitSelected(suit);
  };

  return (
    <SuitsSelectorStyled>
      {
        cards.suits.map(suit => (
          <SelectorContainer
            key={suit.value}
          >
            <input
              type="radio"
              id={`suit-selector-${suit.value}`}
              onChange={() => selectSuit(suit)}
              checked={selectedSuit && selectedSuit.value === suit.value}
            />
            <SuitIcon htmlFor={`suit-selector-${suit.value}`} color={suit.color}>{suit.title}</SuitIcon>
          </SelectorContainer>
        ))
      }
    </SuitsSelectorStyled>
  )
}
