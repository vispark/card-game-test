import styled, {css} from 'styled-components';

export default styled.div`
  display: flex;
  justify-content: space-around;
  width: 300px;
`;

export const SelectorContainer =  styled.div`
  cursor: pointer;
`;

export const SuitIcon = styled.label`
  color: #000;
  cursor: pointer;

  ${props =>
    props.color === 'red' &&
    css`
      color: #f00;
    `};
`;
