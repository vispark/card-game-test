import React from 'react';
import cards from '../../constants/cards';

import CardStyled from './index.styled';

export default function Card({
  number = cards.numbers[0],
  suit = cards.suits[0],

  onCardSelected = () => {},
}) {
  return (
    <CardStyled color={suit.color} onClick={() => onCardSelected({number, suit})}>
      <p>{`${number.title} ${suit.title}`}</p>
    </CardStyled>
  );
}
