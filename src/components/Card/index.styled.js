import styled, {css} from 'styled-components';

export default styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 200px;
  width: 120px;
  min-width: 120px;
  margin: 10px;
  color: #000;
  border: solid 1px #000;
  border-radius: 5px;
  cursor: pointer;
  
  ${props =>
    props.color === 'red' &&
    css`
      color: #f00;
    `};
`;
