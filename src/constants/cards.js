export default {
  numbers: [
    {title: '2', value: 1},
    {title: '3', value: 2},
    {title: '4', value: 3},
    {title: '5', value: 4},
    {title: '6', value: 5},
    {title: '7', value: 6},
    {title: '8', value: 7},
    {title: '9', value: 8},
    {title: '10', value: 9},
    {title: 'J', value: 10},
    {title: 'D', value: 11},
    {title: 'K', value: 12},
    {title: 'A', value: 13},
  ],
  suits: [
    {title: '♣', value: 'club', color: 'black'},
    {title: '♦', value: 'diamond', color: 'red'},
    {title: '♥', value: 'heart', color: 'red'},
    {title: '♠', value: 'spade', color: 'black'},
  ]
}
