export const COUNTRY_TO_LIE = 'IL';

export const Answers = {
  MATCH: 'MATCH',
  NO_MATCH: 'NO MATCH',
  LOWER: 'LOWER',
  HIGHER: 'HIGHER',
};
