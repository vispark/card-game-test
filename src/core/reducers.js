import {combineReducers} from 'redux';

import application from './application/redux/reducer';

export default combineReducers({
  application,
});
