import {get} from '../utils/request';

export const getIpInfoRequest = () =>
  get(`http://ip-api.com/json/`);
