export const INIT = {};

INIT.REQUESTED = 'init/requested';
INIT.FAILED = 'init/failed';
INIT.SUCCEEDED = 'init/succeeded';

export const init = {
  request: () =>
    ({type: INIT.REQUESTED}),

  failed: error =>
    ({type: INIT.FAILED, payload: error}),

  succeeded: (payload) =>
    ({type: INIT.SUCCEEDED, payload}),
};
