import { INIT } from './actions';

const initial = { isError: false };

export default (state = initial, { type, payload }) => {
  switch (type) {
    case INIT.REQUESTED: {
      return state;
    }

    case INIT.SUCCEEDED: {
      return {
        ...state,
        ...payload,
        isError: false
      };
    }

    case INIT.FAILED: {
      return {
        ...state,
        isError: true
      };
    }

    default: {
      return state;
    }
  }
};
