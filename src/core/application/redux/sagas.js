import {
  put,
  takeEvery,
} from 'redux-saga/effects';

import request from '../../utils/request-processing';

import {
  INIT,
  init,
} from './actions';

import {
  getIpInfoRequest
} from '../endpoints';

function* initiate() {
  try {
    yield request.generator(getIpInfoRequest)(init);
  } catch (error) {
    yield put(init.failed(error));
  }
}

export default [
  takeEvery(INIT.REQUESTED, initiate),
];
