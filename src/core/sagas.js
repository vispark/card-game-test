import {all} from 'redux-saga/effects';

import application from './application/redux/sagas';

export default function* sagas() {
  yield all([
    ...application,
  ]);
}
