import {put, call} from 'redux-saga/effects';

export function generator(request, ...params) {
  return function* _make({failed, succeeded}) {
    try {
      const payload = yield call(request, ...params);

      if (payload.error || payload.status >= 300) {
        yield put(failed({status: payload.status, error: payload.error}));

        return void(0);
      }

      yield put(succeeded(payload));

      return payload;
    } catch(error) {
      yield put(failed({error}));
    }
  };
}

export default {
  generator,
};
