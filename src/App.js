import React from 'react';
import { Provider } from 'react-redux';
import { store } from './core/store';

import Game from './containers/Game';

import './App.css';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Game/>
      </div>
    </Provider>
  );
}

export default App;
