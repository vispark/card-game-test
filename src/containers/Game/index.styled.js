import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  
  min-height: 100vh;
`;

export const CardsContainer = styled.div`
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
`;

export const SuitSelectorContainer = styled.div`
  display: flex;
  justify-content: center;
`;
