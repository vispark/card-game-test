import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {init} from '../../core/application/redux/actions';
import cards from '../../constants/cards';
import {COUNTRY_TO_LIE, Answers} from '../../constants';

import Card from '../../components/Card';
import SuitSelector from '../../components/SuitSelector';

import GameStyled, {CardsContainer, SuitSelectorContainer} from './index.styled';

function Game(props) {
  const [target, setTarget] = useState(null);
  const [currentSuit, setSuit] = useState(cards.suits[0]);
  const [result, setResult] = useState({turn: 1});

  const startGame = () => {
    setTarget({
      suit: cards.suits[Math.floor(Math.random() * cards.suits.length)],
      number: cards.numbers[Math.floor(Math.random() * cards.numbers.length)],
    });

    setResult({turn: 1});
  };

  useEffect(() => {
    props.init();
    startGame();
  }, []);

  const selectCard = ({number, suit}) => {
    let isLying = false;

    if (!(result.turn % 3) && props.playerCountry.toUpperCase() === COUNTRY_TO_LIE) {
      isLying = true;
    }

    const Results = {
      MATCH: isLying ? Answers.NO_MATCH : Answers.MATCH,
      NO_MATCH: isLying ? Answers.MATCH : Answers.NO_MATCH,
      LOWER: isLying ? Answers.HIGHER : Answers.LOWER,
      HIGHER: isLying ? Answers.LOWER : Answers.HIGHER,
    };

    setResult({
      turn: result.turn + 1,
      suit: suit.value === target.suit.value ? Results.MATCH : Results.NO_MATCH,
      number: number.value === target.number.value ?
        Results.MATCH
        : number.value < target.number.value
          ? Results.HIGHER
          : Results.LOWER,
    });

    if (suit.value === target.suit.value && number.value === target.number.value) {
      alert('Congratulations!');
      startGame();
    }
  };

  return (
    <GameStyled>
      <h1>{`Turn: ${result.turn}`}</h1>

      <SuitSelectorContainer>
        <SuitSelector onSuitSelected={setSuit}/>
      </SuitSelectorContainer>

      <CardsContainer>
        {
          cards.numbers.map(number => (
            <Card
              key={number.value}
              suit={currentSuit}
              number={number}
              onCardSelected={selectCard}
            />
          ))
        }
      </CardsContainer>

      {result.number && result.suit ?
        <p>{`Result: Number: ${result.number}, Suit: ${result.suit}`}</p>
      : <p>Select the card to start the Game</p>}

    </GameStyled>
  );
}

export default connect(
  ({application}) => ({
    playerCountry: application.countryCode || 'undetermined'
  }),
  dispatch => ({
    init: () => dispatch(init.request()),
  })
)(Game);
